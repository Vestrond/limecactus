/**
 * Created by CSH on 18.04.2017.
 */
"use strict";

/**
 * LimeCactus - 'class' that contain DOM Element, and working with him.
 * @private
 */
class LimeCactus {
    /**
     * Get DOM Element and save it inside.
     *
     * @param {Element} arg - DOM Element
     */
    constructor (arg){
        this.htmlElement = arg;
    }

    /**
     * Get DOM Element's html code
     * @return {string} - html code
     */
    get html(){
        return this.htmlElement.outerHTML;
    }

    /**
     *
     * @return {string}
     */
    toString(){
        return this.html;
    }

    /**
     * Get DOM Element's classes
     *
     * @return {string[]}
     */
    get classes(){
        let classes_ = [];
        let cList = this.htmlElement.classList;

        for(let i = 0; i < cList.length; i++){
            classes_.push(cList[i]);
        }

        return classes_;
    }

    /**
     * Set DOM Element's classes
     *
     * @param {string[]} arg
     */
    set classes(arg){
        if(typeof(arg) === "string"){
            this.clearClasses();
            this.addClass(arg);

            return true;
        }
        else if(arg instanceof Array){
            this.clearClasses();

            for(let i = 0; i < arg.length; i++){
                this.htmlElement.classList.add(arg[i]);
            }

            return true;
        }
        return null;
    }

    /**
     * Add class to DOM Element
     *
     * @param {(string|string[])} arg
     * @return {(boolean|null)} - 'true' if function complete, 'null' - if bad arg.
     */
    addClass(arg){
        if(typeof(arg) === "string"){
            this.htmlElement.classList.add(arg);
            return true;
        }
        else if(arg instanceof Array){
            for(let i = 0; i < arg.length; i++){
                this.addClass(arg[i]);
            }

            return true;
        }
        return null;
    }

    /**
     * Check that DOM Element have a class
     *
     * @param {(string|string[]|undefined)} arg
     * @return {(boolean|null)} - 'true' if function complete, 'null' - if bad arg.
     */
    hasClass(arg=undefined){
        if(typeof(arg) === "string"){
            for(let i = 0; i < this.htmlElement.classList.length; i++){
                if(this.htmlElement.classList[i] === arg)
                    return true;
            }
            return false;
        }
        else if(arg instanceof Array){
            if(arg.length === 0)
                return this.hasClass();

            let stat = true;
            for(let i = 0; i < arg.length; i++){
                if(!this.hasClass(arg[i]))
                    stat = false;
            }

            return stat;
        }
        else if(arg === undefined){
            return this.htmlElement.classList.length > 0;
        }
        return null;
    }

    /**
     * Remove class to DOM Element
     *
     * @param {(string|string[])} arg
     * @return {(boolean|null)} - 'true' if function complete, 'null' - if bad arg.
     */
    removeClass(arg){
        if(typeof(arg) === "string"){
            this.htmlElement.classList.remove(arg);

            if(this.htmlElement.classList.length === 0)
                this.clearClasses();

            return true;
        }
        else if(arg instanceof Array){
            for(let i = 0; i < arg.length; i++){
                this.removeClass(arg[i]);
            }

            return true;
        }
        return null;
    }

    /**
     * Toggle class in DOM Element
     *
     * @param {(string|string[])} arg
     * @return {(boolean|null)} - 'true' if function complete, 'null' - if bad arg.
     */
    toggleClass(arg){
        if(typeof(arg) === "string"){
            this.htmlElement.classList.toggle(arg);

            if(this.htmlElement.classList.length === 0)
                this.clearClasses();

            return true;
        }
        else if(arg instanceof Array){
            for(let i = 0; i < arg.length; i++){
                this.toggleClass(arg[i]);
            }

            return true;
        }
        return null;
    }

    /**
     * Clear all DOM Element's classes
     *
     * @return {boolean|null} - 'true' if function complete, else 'false'.
     */
    clearClasses(){
        let attr = this.htmlElement.attributes;
        if(attr.getNamedItem("class") !== null){
            attr.removeNamedItem("class");
            return true;
        }
        return null;
    }

    /**
     * Sort each DOM Element's class
     *
     * @return {boolean|null}
     */
    sortClasses(){
        let ln = this.htmlElement.classList.length;

        if(ln > 0){
            let classes = this.classes;

            this.clearClasses();
            classes.sort();

            this.classes = classes;

            return true;
        }
        return null;
    }

    /**
     * Get DOM Element's attributes
     *
     * @return {ElementAttributes}
     */
    get attributes(){
        return aT(this.htmlElement.attributes);
    }

    /**
     * Get DOM Element's attributes
     *
     * @param {ElementAttributes} arg
     */
    set attributes(arg){
        if(arg instanceof ElementAttributes){
            this.clearAttributes();

            for(let elem of arg){
                this.htmlElement.setAttribute(elem.name, elem.value);
            }
        }
    }

    /**
     * Get attribute from DOM Element when 1 argument taken
     * Set attribute to DOM Element when 2 arguments taken, or 1 ElementAttribute(s).
     *
     * @param {(string|ElementAttribute|ElementAttributes)} arg
     * @param {(string|undefined)} value
     * @return {(boolean|string|null)} - 'true' if function complete, 'string' when need to get, 'null' - if bad arg.
     */
    attribute(arg, value=undefined){
        if(typeof(arg) === "string" && typeof (value) === "string"){
            this.htmlElement.setAttribute(arg, value);
            return true;
        }
        else if(arg instanceof ElementAttribute && value === undefined) {
            this.htmlElement.setAttribute(arg.name, arg.value);
            return true;
        }
        else if(arg instanceof ElementAttributes && value === undefined){
            for(let elem of arg){
                this.htmlElement.setAttribute(elem.name, elem.value);
            }
            return true;
        }
        else if(typeof(arg) === "string" && value === undefined){
            return this.htmlElement.getAttribute(arg);
        }
        return null;
    }

    /**
     * Remove attribute by name or ElementAttribute(s)
     *
     * @param {String|ElementAttribute|ElementAttributes} arg - Name of attribute
     */
    removeAttribute(arg){
        if(typeof (arg) === "string"){
            this.htmlElement.removeAttribute(arg);
            return true;
        }
        else if(arg instanceof ElementAttribute){
            this.htmlElement.removeAttribute(arg.name);
            return true;
        }
        else if(arg instanceof ElementAttributes){
            for(let elem of arg){
                this.htmlElement.removeAttribute(elem.name);
            }
            return true;
        }
        return null;
    }

    /**
     * Clear all attributes from DOM element
     *
     * @return {boolean}
     */
    clearAttributes(){
        for(let attr of this.attributes){
            this.htmlElement.removeAttribute(attr.name);
        }
        return true;
    }

}

/**
 * ManyLimeCacti - container for LimeCactus.
 * We can get ManyLimeCacti from 'cC()' function.
 *
 * @private
 * @param {object[]} cacti - _Array of Elements_
 */
class ManyLimeCacti{
    /**
     * Fill 'this.cacti' with param's Array.
     * Fill ManyLimeCacti's indexes ([0], [1], ...) with Elements
     * Fill 'this.length'
     *
     * @param {Element[]} cacti - _Array of Elements_
     */
    constructor(cacti){
        this.cacti = cacti;

        for(let cactus in cacti) {
            if (cacti.hasOwnProperty(cactus))
                this[cactus] = new LimeCactus(cacti[cactus]);
        }
    }

    /**
     *
     * @return {Number}
     */
    get length(){
        return this.cacti.length;
    }

    /**
     * If container ManyLimeCacti is empty then 'true'.
     *
     * @return {boolean}
     */
    get empty(){
        return this.length === 0;
    }

    /**
     * Get html code each ManyLimeCacti's element.
     *
     * @return {string[]}
     */
    get html(){
        let htmls = [];
        for(let cactus of this.cacti){
            if(this.cacti.hasOwnProperty(cactus))
                htmls.push(this[cactus].html);
        }
        return htmls;
    }

    /**
     * Get ManyLimeCacti's elements' classes as string.
     *
     * @return {Array} - [String[], ...]
     */
    get classes(){
        let classes = [];
        for(let cactus in this.cacti){
            if(this.cacti.hasOwnProperty(cactus))
                classes.push(this[cactus].classes);
        }
        return classes;
    }

    /**
     * Set ManyLimeCacti's elements' classes
     *
     * @param {Array} arg. String[] or [String[], ...]
     */
    set classes(arg){
        if(typeof (arg[0]) === "string"){
            for (let cactus in this.cacti) {
                if (this.cacti.hasOwnProperty(cactus))
                    this[cactus].classes = arg;
            }
        }
        else if(arg.length === this.length){
            for(let i = 0; i < arg.length; i++){
                this[i].classes = arg[i];
            }
        }
    }

    /**
     * Add class to each ManyLimeCacti's element
     *
     * @param {(string|string[])} arg
     */
    addClass(arg){
        for(let cactus in this.cacti){
            if(this.cacti.hasOwnProperty(cactus))
                this[cactus].addClass(arg);
        }
    }

    /**
     * Check ManyLimeCacti's elements for the presence of the class.
     * Have a "AND" logic.
     *
     * @param {(string|string[])} arg
     * @return {boolean[]}
     */
    hasClass(arg){
        let who_has = [];
        for(let cactus in this.cacti){
            if(this.cacti.hasOwnProperty(cactus))
                who_has.push(this[cactus].hasClass(arg));
        }
        return who_has;
    }

    /**
     * Remove from ManyLimeCacti's elements class (if they have)
     *
     * @param {(string|string[])} arg
     */
    removeClass(arg){
        for(let cactus in this.cacti){
            if(this.cacti.hasOwnProperty(cactus))
                this[cactus].removeClass(arg);
        }
    }

    /**
     * Toggle ManyLimeCacti's elements class
     *
     * @param {(string|string[])} arg
     */
    toggleClass(arg){
        for(let cactus in this.cacti){
            if(this.cacti.hasOwnProperty(cactus))
                this[cactus].toggleClass(arg);
        }
    }

    /**
     * Clear all classes into each ManyLimeCacti's element
     */
    clearClasses(){
        for(let cactus in this.cacti){
            if(this.cacti.hasOwnProperty(cactus))
                this[cactus].clearClasses();
        }
    }

    /**
     * Sort all classes into each ManyLimeCacti's element
     */
    sortClasses(){
        for(let cactus in this.cacti){
            if(this.cacti.hasOwnProperty(cactus)){
                console.log(cactus);
                this[cactus].sortClasses();}
        }
    }

    /**
     *
     * @return {ElementAttributes[]}
     */
    get attributes(){
        let attributes_array = [];

        for(let elem of this){
            attributes_array.push(elem.attributes);
        }

        return attributes_array;
    }

    /**
     *
     * @param {ElementAttributes[]} arg - ElementAttributes[] or ElementAttributes
     */
    set attributes(arg){
        if(arg instanceof Array && arg.length === this.length){
            for(let i = 0; i < arg.length; i++){
                if(arg[i] instanceof ElementAttributes){
                    this[i].attributes = arg[i];
                }
            }
        }
        else if(arg instanceof ElementAttributes){
            for(let elem of this){
                elem.attributes = arg;
            }
        }
    }

    /**
     *
     * @param {String|ElementAttribute|ElementAttributes} arg
     * @param {string|undefined} value
     * @return {boolean|String[]|null}
     */
    attribute(arg, value=undefined){
        if(typeof arg === "string" && value===undefined){
            let values_array = [];

            for(let elem of this){
                values_array.push(elem.attribute(arg));
            }

            return values_array;
        }

        for(let elem of this){
            if(elem.attribute(arg, value) === null){
                return null;
            }
        }
        return true;
    }
}

/**
 * Class for providing Attributes
 *
 * @private
 */
class ElementAttribute{
    /**
     * Create Element attribute.
     *
     * @param {string} name - attribute name
     * @param {string|null} value - attribute value
     */
    constructor(name, value){
        if(typeof (name) === "string" && typeof (value) === "string") {
            this.name = name;
            this.value = value;
        }
        if(typeof (name) === "string" && value === undefined){
            this.name = name;
            this.value = "";
        }
    }

    /**
     * See attribute like in html-code
     *
     * @return string - text view for attributes
     */
    toString(){
        return `${this.name}="${this.value}"`;
    }

    /**
     * Add value to attribute
     *
     * @param {String} arg
     */
    add(arg){
        if(typeof (arg) === "string"){
            this.value += ` ${arg}`;
            return true;
        }
        return false;
    }

    /**
     * Remove from attribute
     *
     * @param {String} arg
     */
    remove(arg){
        if(typeof (arg) === "string"){
            let temp = this.value.split(" ");
            temp.push(arg);
            this.value = temp.join("");
        }
    }
}

/**
 * Class-collection with ElementAttributes
 *
 * @private
 */
class ElementAttributes{
    /**
     * Create collection
     *
     * @param {ElementAttribute|ElementAttribute[]} arg
     */
    constructor(arg){
        if(arg instanceof Array){
            this.attributes = arg;
        }
        else if (arg instanceof ElementAttribute){
            this.attributes = [arg];
        }

        for(let i in this.attributes){
            if(this.attributes.hasOwnProperty(i)){
                this[i] = this.attributes[i];
            }
        }
    }

    /**
     * See attributes like in html-code
     *
     * @return string - text view for attributes
     */
    toString() {
        let temp_lines = [];

        for (let attr of this.attributes) {
            temp_lines.push(attr.toString());
        }

        return temp_lines.join(" ");
    }

    /**
     * Get each attribute's name from collection
     *
     * @return {String[]}
     */
    get names(){
        let names_array = [];

        for(let attribute of this.attributes){
            names_array.push(attribute.name);
        }

        return names_array;
    }
}


/**
 * General function for working with LimeCactus.
 *
 * @param {(string|undefined)} selector - Like "li.big" or "div#red"...
 * @return {ManyLimeCacti} ManyLimeCacti
 */
function cC(selector=undefined) {
    if(selector === undefined)
        selector = "html";
    if(typeof(selector) !== "string")
        return null;

    let elements = document.querySelectorAll(selector);

    let cacti = [];                     // Array with elements' DOM

    for(let elem = 0; elem < elements.length; elem++){
        cacti.push(elements[elem]);
    }

    return new ManyLimeCacti(cacti);
}

/**
 * Function for working with html-attributes
 *
 * @param {string|Object|NamedNodeMap} arg - name of attribute or dictonary
 * @param {string|null} value - value of attribute if arg - name
 * @return {ElementAttributes}
 */
function aT(arg, value=undefined) {
    if(typeof (arg) === "string" && typeof (value) === "string"){
        return new ElementAttributes(new ElementAttribute(arg, value));
    }
    else if(arg instanceof NamedNodeMap){
        let attributes = [];

        for(let attr of arg){
            attributes.push(new ElementAttribute(attr.name, attr.value));
        }

        return new ElementAttributes(attributes);
    }
    else if(arg instanceof Object && value===undefined && Object.keys(arg).length > 0){
        let attributes = [];

        for(let name in arg){
            if(arg.hasOwnProperty(name))
                attributes.push(new ElementAttribute(name, arg[name]));
        }

        return new ElementAttributes(attributes);
    }
}

/**
 * SHADOW CACTUS (Experiment)
 * Functional the same as cC(), but isolates the selected items in the shade.
 * Check your browser for support ShadowDOM.
 *
 * @param {(string|undefined)} selector - Like "li.big" or "div#red"...
 * @return {ManyLimeCacti} ManyLimeCacti
 */
function sC(selector) {
    if(selector === undefined)
        selector = "html";
    if(typeof(selector) !== "string")
        return null;

    let elements = document.querySelectorAll(selector);

    let cacti = [];

    for(let elem = 0; elem < elements.length; elem++){
        if(elements[elem].parentElement.shadowRoot === null){
            elements[elem].parentElement.createShadowRoot().innerHTML = "<content></content>";
            // elements
        }
        cacti.push(elements[elem]);
    }

    return new ManyLimeCacti(cacti);
}